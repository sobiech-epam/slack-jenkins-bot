package com.usta.slackjenkinsbot;

import com.usta.slackjenkinsbot.jenkins.JenkinsService;
import com.usta.slackjenkinsbot.slack.SlackResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/slack")
@RequiredArgsConstructor
class SlackController {

    private static final String WHITE_SPACE = " ";

    private final JenkinsService jenkinsService;

    @PostMapping(value = "/status", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    SlackResponse onReceiveStatusSlashCommand(@RequestParam("team_id") String teamId,
                                                                @RequestParam("team_domain") String teamDomain,
                                                                @RequestParam("channel_id") String channelId,
                                                                @RequestParam("channel_name") String channelName,
                                                                @RequestParam("user_id") String userId,
                                                                @RequestParam("user_name") String userName,
                                                                @RequestParam("command") String command,
                                                                @RequestParam("text") String text,
                                                                @RequestParam("response_url") String responseUrl) {
        return jenkinsService.getRunningJobExecutionsForEnv(command);
    }

    @PostMapping(value = "/run", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    SlackResponse onReceiveRunSlashCommand(@RequestParam("team_id") String teamId,
                                           @RequestParam("team_domain") String teamDomain,
                                           @RequestParam("channel_id") String channelId,
                                           @RequestParam("channel_name") String channelName,
                                           @RequestParam("user_id") String userId,
                                           @RequestParam("user_name") String userName,
                                           @RequestParam("command") String command,
                                           @RequestParam("text") String text,
                                           @RequestParam("response_url") String responseUrl) {
        String env = command.split(WHITE_SPACE)[0];
        String suite = command.substring(env.length() + 1);

        return jenkinsService.triggerJob(env, suite);
    }
}
