package com.usta.slackjenkinsbot.config;

import com.cdancy.jenkins.rest.JenkinsClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usta.slackjenkinsbot.util.JsonUtils;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Configuration
public class AppConfig {

    Logger logger = LoggerFactory.getLogger(AppConfig.class);

    private String uriScheme;
    @Value("${jenkins.host}")
    private String jenkinsHost;
    @Value("${jenkins.user}")
    private String jenkinsUsername;
    @Value("${jenkins.password}")
    private String jenkinsPassword;
    private BotConfig botConfig;

    public AppConfig(@Value("${uri.scheme}") String uriScheme,
                     @Value("${jenkins.host}") String jenkinsHost,
                     @Value("${jenkins.user}") String jenkinsUsername,
                     @Value("${jenkins.password}") String jenkinsPassword) {

        this.uriScheme = uriScheme;
        this.jenkinsHost = jenkinsHost;
        this.jenkinsUsername = jenkinsUsername;
        this.jenkinsPassword = jenkinsPassword;
    }

    @Bean
    JenkinsClient jenkinsClient() {

        String auth = String.format("%s:%s", jenkinsUsername, jenkinsPassword);
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.US_ASCII));

        return JenkinsClient.builder()
                .endPoint(uriScheme+ "://" + jenkinsHost)
                .credentials(new String(encodedAuth))
                .build();
    }

    @Bean
    @Scope("singleton")
    BotConfig botConfig() {
        if (this.botConfig == null) {
            final String configJson = getBotConfigFromJson();
            this.botConfig = readBotConfig(configJson);
        }

        return this.botConfig;
    }

    private String getBotConfigFromJson() {
        String configJson = null;
        try {
            configJson = JsonUtils.getJsonResource(AppConfig.class, "config.json");
        } catch (IOException e) {
            logger.error("Couldn't read JSON configuration");
        }
        return configJson;
    }

    private BotConfig readBotConfig(String configJson) {
        BotConfig config = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            config = objectMapper.readValue(configJson, BotConfig.class);
        } catch (JsonProcessingException e) {
            logger.error("Couldn't process JSON configuration");
        }
        return config;
    }

    public Optional<JobConfig> getJobConfigByEnv(String env) {
        return this.botConfig
                .getConfig()
                .stream()
                .filter(config -> config.getEnvironment().equalsIgnoreCase(env))
                .findFirst();
    }

    public Optional<JobConfig> getJobConfigByEnvAndSuite(String env, String suite) {
        return this.botConfig
                .getConfig()
                .stream()
                .filter(config -> config.getEnvironment().equalsIgnoreCase(env) && config.getSuite().equalsIgnoreCase(suite))
                .findFirst();
    }

    public BotConfig getBotConfig() {
        return botConfig;
    }
}
