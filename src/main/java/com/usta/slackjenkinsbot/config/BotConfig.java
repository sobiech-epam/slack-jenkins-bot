package com.usta.slackjenkinsbot.config;

import lombok.Data;

import java.util.List;

@Data
public class BotConfig {

    private String slackToken;
    private String jenkinsToken;
    private String view;
    private List<JobConfig> config;
}
