package com.usta.slackjenkinsbot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.zalando.logbook.httpclient.LogbookHttpRequestInterceptor;
import org.zalando.logbook.httpclient.LogbookHttpResponseInterceptor;

@Configuration
public class RestTemplateConfig extends RestTemplateFactory {

    public RestTemplateConfig(LogbookHttpRequestInterceptor logbookHttpRequestInterceptor,
                              LogbookHttpResponseInterceptor logbookHttpResponseInterceptor) {
        super(logbookHttpRequestInterceptor, logbookHttpResponseInterceptor);
    }

    @Bean
    public RestTemplate jenkinsRestTemplate() {
        return new RestTemplate(getClientHttpRequest());
    }
}
