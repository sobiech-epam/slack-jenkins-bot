package com.usta.slackjenkinsbot.config;

import lombok.Data;

@Data
public class JobConfig {

    private String environment;
    private String suite;
    private String jobId;
}
