package com.usta.slackjenkinsbot.util;

import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class JsonUtils {

    public static String getJsonResource(Class clazz, String file) throws IOException {
        try (InputStream inputStream = findResource(clazz, "/" + file)) {
            if (inputStream == null) {
                throw new FileNotFoundException("Can not find resource file: " + file + " for class " + clazz.getName());
            }
            return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        }
    }

    private static InputStream findResource(Class clazz, String file) throws IOException {
        InputStream resource = clazz.getResourceAsStream(file);
        return resource;
    }

    public static String getJsonResource(Class clazz, String classPackage, String file) throws IOException {
        return getJsonResource(clazz, String.format("%s/%s", classPackage, file));
    }
}
