package com.usta.slackjenkinsbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SlackJenkinsBotApplication extends SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlackJenkinsBotApplication.class, args);
	}
}
