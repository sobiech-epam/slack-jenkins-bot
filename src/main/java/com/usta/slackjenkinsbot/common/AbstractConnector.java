package com.usta.slackjenkinsbot.common;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.StandardCharsets;

public abstract class AbstractConnector {

    protected HttpHeaders createHeaderWithBasicAuth(String username, String password) {
        HttpHeaders headers = new HttpHeaders();
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.US_ASCII));
        String authHeader = "Basic " + new String(encodedAuth);
        headers.set("Authorization", authHeader);
        return headers;
    }

    protected String buildUri(String uriScheme, String host, String... referenceDataResource) {
        return UriComponentsBuilder.newInstance()
                .scheme(uriScheme)
                .host(host)
                .pathSegment(referenceDataResource)
                .build().toString();
    }
}
