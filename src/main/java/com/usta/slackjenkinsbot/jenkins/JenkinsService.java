package com.usta.slackjenkinsbot.jenkins;

import com.usta.slackjenkinsbot.task.TaskExecutor;
import com.usta.slackjenkinsbot.slack.SlackResponseType;
import com.usta.slackjenkinsbot.config.AppConfig;
import com.usta.slackjenkinsbot.config.JobConfig;
import com.usta.slackjenkinsbot.slack.SlackResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JenkinsService {

    Logger logger = LoggerFactory.getLogger(JenkinsService.class);

    private static final String PARAM_NAME_ENVIRONMENT = "ENVIRONMENT";

    private final JenkinsConnector jenkinsConnector;
    private final JenkinsBuildInfoConverter buildConverter;
    private final TaskExecutor taskExecutor;
    private final JenkinsJob jenkinsJob;
    private final AppConfig appConfig;

    public SlackResponse getRunningJobExecutionsForEnv(String env) {
        SlackResponse response;

        if (appConfig.getJobConfigByEnv(env).isPresent()) {
            final JenkinsJobList jobs = jenkinsConnector.getAllJobs();
            final List<JenkinsJobInfo> jobInfos = getJobInfos(jobs);
            List<JenkinsBuildInfo> filteredBuilds = getRunningBuildsForEnv(env, jobInfos);
            if (filteredBuilds.isEmpty()) {
                response = buildResponse("No executions scheduled", SlackResponseType.SUCCESS);
            } else {
                response = buildRunningBuildsResponse(env, filteredBuilds);
            }
        } else {
            String msg = String.format("%s is not described in configuration", env);
            response = buildResponse(msg, SlackResponseType.FAILURE);
        }

        return response;
    }

    private List<JenkinsJobInfo> getJobInfos(JenkinsJobList jobs) {
        return jobs
                .getJobs()
                .stream()
                .map(job -> jenkinsConnector.getJobInfo(job.getName()))
                .collect(Collectors.toList());
    }

    private List<JenkinsBuildInfo> getRunningBuildsForEnv(String env, List<JenkinsJobInfo> jobs) {
        return getBuildInfosForRunningBuilds(jobs)
                .stream()
                .filter(build -> build.getActions() != null && !build.getActions().isEmpty())
                .filter(build -> build.getActions().get(0).getParameters() != null && !build.getActions().get(0).getParameters().isEmpty())
                .filter(build -> build.getActions().get(0).getParameters()
                        .stream()
                        .anyMatch(param -> param.getName().equalsIgnoreCase(PARAM_NAME_ENVIRONMENT) && param.getValue().equalsIgnoreCase(env)))
                .collect(Collectors.toList());
    }

    private List<JenkinsBuildInfo> getBuildInfosForRunningBuilds(List<JenkinsJobInfo> jobs) {
        return jobs
                .stream()
                .filter(jenkinsJobInfo -> !jenkinsJobInfo.getBuilds().isEmpty())
                .map(job -> jenkinsConnector.getBuildInfo(job.getName(), getLastBuildNumber(job)))
                .filter(JenkinsBuildInfo::getIsBuilding)
                .collect(Collectors.toList());
    }

    private String getLastBuildNumber(JenkinsJobInfo jobInfo) {
        return jobInfo.getLastSuccessfulBuild() != null
                ? jobInfo.getLastSuccessfulBuild().getNumber() : jobInfo.getBuilds().get(0).getNumber();
    }

    private JenkinsRunningBuildListResponse buildRunningBuildsResponse(String env, List<JenkinsBuildInfo> builds) {
        List<JenkinsBuildInfoDto> buildInfoDtos = builds
                .stream()
                .map(buildConverter::convert)
                .collect(Collectors.toList());

        return JenkinsRunningBuildListResponse
                .builder()
                .environment(env)
                .builds(buildInfoDtos)
                .build();
    }

    public SlackResponse triggerJob(String env, String suite) {
        SlackResponse response;
        Optional<JobConfig> jobConfig = appConfig.getJobConfigByEnvAndSuite(env, suite);

        if (jobConfig.isPresent()) {
            final String jobName = jobConfig.get().getJobId();
            JenkinsJobInfo jobInfo = jenkinsConnector.getJobInfo(jobName);
            JenkinsBuildInfo lastBuildInfo = getLastBuild(jobName, jobInfo);
            response = runJobIfNotStarted(jobInfo, lastBuildInfo);
        } else {
            String msg = String.format("'%s' environment and '%s' jobId do not match", env, suite);
            response = buildResponse(msg, SlackResponseType.FAILURE);
        }

        return response;
    }

    private JenkinsBuildInfo getLastBuild(String jobName, JenkinsJobInfo jobInfo) {
        JenkinsBuildInfo buildInfo = JenkinsBuildInfo.builder().build();
        if (jobInfo != null) {
            String buildNumber = getLastBuildNumber(jobInfo);
            buildInfo = jenkinsConnector.getBuildInfo(jobName, buildNumber);
        }
        return buildInfo;
    }

    private SlackResponse runJobIfNotStarted(JenkinsJobInfo jobInfo, JenkinsBuildInfo lastBuildInfo) {
        SlackResponse response;

        if (lastBuildInfo.getIsBuilding() != null && lastBuildInfo.getIsBuilding()) {
            response = buildResponse("Job is running", SlackResponseType.SUCCESS);
        } else {
            final String jobName = jobInfo.getName();
            jenkinsConnector.runJob(jobName);
            pollJenkinsForJobResults(jobName);
            response = buildResponse("Job started", SlackResponseType.SUCCESS);
        }

        return response;
    }

    private void pollJenkinsForJobResults(String jobName) {
        Integer number = jenkinsConnector.getLastBuildNumber(jobName);
        jenkinsJob.setName(jobName);
        jenkinsJob.setNumber(String.valueOf(number));
        taskExecutor.executeJob(jenkinsJob);
    }

    private SlackResponse buildResponse(String msg, SlackResponseType responseType) {
        return SlackResponse
                .builder()
                .responseType(responseType.toString())
                .text(msg)
                .build();
    }
}
