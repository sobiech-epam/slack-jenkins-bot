package com.usta.slackjenkinsbot.jenkins;

import com.usta.slackjenkinsbot.task.CancelTaskSchedulerEvent;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JenkinsJob implements Runnable {

    Logger logger = LoggerFactory.getLogger(JenkinsJob.class);

    private final JenkinsConnector jenkinsConnector;
    private final ApplicationEventPublisher applicationEventPublisher;

    private String name;
    private String number;

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public void run() {
        JenkinsBuildInfo buildInfo = jenkinsConnector.getBuildInfo(name, number);
        logger.info(String.format("Job '%s' in progress: %s", name, buildInfo.getIsBuilding()));
        if (!buildInfo.getIsBuilding()) {
            String msg = String.format("Job with name %s finished. Cancelling scheduled polling task", name);
            applicationEventPublisher.publishEvent(new CancelTaskSchedulerEvent(msg));
        }
    }
}
