package com.usta.slackjenkinsbot.jenkins;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JenkinsJobList {

    @JsonProperty("jobs")
    private List<JenkinsJobInfo> jobs;
}
