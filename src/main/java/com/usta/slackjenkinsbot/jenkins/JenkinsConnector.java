package com.usta.slackjenkinsbot.jenkins;

import com.cdancy.jenkins.rest.JenkinsClient;
import com.cdancy.jenkins.rest.domain.common.IntegerResponse;
import com.cdancy.jenkins.rest.domain.job.BuildInfo;
import com.cdancy.jenkins.rest.domain.job.JobInfo;
import com.usta.slackjenkinsbot.common.AbstractConnector;
import com.usta.slackjenkinsbot.config.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class JenkinsConnector extends AbstractConnector {

    Logger logger = LoggerFactory.getLogger(JenkinsConnector.class);

    private static final String PATH_SEGMENT_VIEW = "view";
    private static final String PATH_SEGMENT_JOB = "job";
    private static final String PATH_SEGMENT_API = "api";
    private static final String PATH_SEGMENT_JSON = "json";

    private final RestTemplate jenkinsRestTemplate;
    private final JenkinsClient jenkinsClient;
    private final AppConfig appConfig;
    private final String uriScheme;
    private final String jenkinsHost;
    private final String jenkinsUsername;
    private final String jenkinsPassword;
    private final ApplicationEventPublisher applicationEventPublisher;

    JenkinsConnector(RestTemplate jenkinsRestTemplate,
                     JenkinsClient jenkinsClient,
                     AppConfig appConfig,
                     @Value("${uri.scheme}") String uriScheme,
                     @Value("${jenkins.host}") String jenkinsHost,
                     @Value("${jenkins.user}") String jenkinsUsername,
                     @Value("${jenkins.password}") String jenkinsPassword,
                     ApplicationEventPublisher applicationEventPublisher) {
        this.jenkinsRestTemplate = jenkinsRestTemplate;
        this.jenkinsClient = jenkinsClient;
        this.appConfig = appConfig;
        this.uriScheme = uriScheme;
        this.jenkinsHost = jenkinsHost;
        this.jenkinsUsername = jenkinsUsername;
        this.jenkinsPassword = jenkinsPassword;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public JenkinsJobInfo getJobInfo(String suite) {
        final String jenkinsUri = buildUri(
                uriScheme,
                jenkinsHost,
                PATH_SEGMENT_VIEW,
                appConfig.getBotConfig().getView(),
                PATH_SEGMENT_JOB,
                suite,
                PATH_SEGMENT_API,
                PATH_SEGMENT_JSON
        );

        HttpEntity<String> httpEntity = new HttpEntity<>(createHeaderWithBasicAuth(jenkinsUsername, jenkinsPassword));
        return jenkinsRestTemplate.exchange(jenkinsUri, HttpMethod.GET, httpEntity, JenkinsJobInfo.class).getBody();
    }

    public JenkinsBuildInfo getBuildInfo(String suite, String buildNumber) {
        final String jenkinsUri = buildUri(
                uriScheme,
                jenkinsHost,
                PATH_SEGMENT_VIEW,
                appConfig.getBotConfig().getView(),
                PATH_SEGMENT_JOB,
                suite,
                buildNumber,
                PATH_SEGMENT_API,
                PATH_SEGMENT_JSON
        );

        HttpEntity<String> httpEntity = new HttpEntity<>(createHeaderWithBasicAuth(jenkinsUsername, jenkinsPassword));
        return jenkinsRestTemplate.exchange(jenkinsUri, HttpMethod.GET, httpEntity, JenkinsBuildInfo.class).getBody();
    }

    public JenkinsJobList getAllJobs() {
        final String jenkinsUri = buildUri(
                uriScheme,
                jenkinsHost,
                PATH_SEGMENT_VIEW,
                appConfig.getBotConfig().getView(),
                PATH_SEGMENT_API,
                PATH_SEGMENT_JSON
        );

        HttpEntity<String> httpEntity = new HttpEntity<>(createHeaderWithBasicAuth(jenkinsUsername, jenkinsPassword));
        return jenkinsRestTemplate.exchange(jenkinsUri, HttpMethod.GET, httpEntity, JenkinsJobList.class).getBody();
    }

    public IntegerResponse runJob(String name) {
       return jenkinsClient
               .api()
               .jobsApi()
               .buildWithParameters(null, name, null);
    }

    public Integer getLastBuildNumber(String jobName) {
        return jenkinsClient
                .api()
                .jobsApi()
                .lastBuildNumber(null, jobName);
    }
}
