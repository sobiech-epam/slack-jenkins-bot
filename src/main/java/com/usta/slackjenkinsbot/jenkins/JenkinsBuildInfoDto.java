package com.usta.slackjenkinsbot.jenkins;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JenkinsBuildInfoDto {

    @JsonProperty("number")
    private String number;
    @JsonProperty("fullDisplayName")
    private String fullDisplayName;
    @JsonProperty("building")
    private Boolean isBuilding;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("estimatedDuration")
    private String estimatedDuration;
    @JsonProperty("result")
    private String result;
    @JsonProperty("timestamp")
    private Timestamp timestamp;
}
