package com.usta.slackjenkinsbot.jenkins;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JenkinsBuildInfo {

    @JsonProperty("number")
    private String number;
    @JsonProperty("fullDisplayName")
    private String fullDisplayName;
    @JsonProperty("building")
    private Boolean isBuilding;
    @JsonProperty("duration")
    private long duration;
    @JsonProperty("estimatedDuration")
    private long estimatedDuration;
    @JsonProperty("result")
    private String result;
    @JsonProperty("timestamp")
    private Timestamp timestamp;
    @JsonProperty("actions")
    private List<JenkinsJobAction> actions;
}
