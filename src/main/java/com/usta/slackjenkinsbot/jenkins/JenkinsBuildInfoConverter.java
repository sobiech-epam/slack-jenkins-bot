package com.usta.slackjenkinsbot.jenkins;


import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JenkinsBuildInfoConverter implements Converter<JenkinsBuildInfo, JenkinsBuildInfoDto> {

    @Override
    public JenkinsBuildInfoDto convert(JenkinsBuildInfo source) {
        return JenkinsBuildInfoDto.builder()
                .number(source.getNumber())
                .fullDisplayName(source.getFullDisplayName())
                .isBuilding(source.getIsBuilding())
                .duration(convertDuration(source.getDuration()))
                .estimatedDuration(convertDuration(source.getEstimatedDuration()))
                .result(source.getResult())
                .timestamp(source.getTimestamp())
                .build();
    }

    private String convertDuration(long duration) {
        return DurationFormatUtils.formatDurationWords(duration, true, true);
    }
}
