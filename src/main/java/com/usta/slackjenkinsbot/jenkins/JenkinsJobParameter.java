package com.usta.slackjenkinsbot.jenkins;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JenkinsJobParameter {

    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private String value;
}
