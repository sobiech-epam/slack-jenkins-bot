package com.usta.slackjenkinsbot.jenkins;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.usta.slackjenkinsbot.slack.SlackResponse;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JenkinsRunningBuildListResponse extends SlackResponse {

    private String environment;
    private List<JenkinsBuildInfoDto> builds;
}
