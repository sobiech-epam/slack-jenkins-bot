package com.usta.slackjenkinsbot.task;

import org.springframework.context.ApplicationEvent;

public class CancelTaskSchedulerEvent extends ApplicationEvent {

    public CancelTaskSchedulerEvent(Object source) {
        super(source);
    }
}
