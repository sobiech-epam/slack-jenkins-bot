package com.usta.slackjenkinsbot.task;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TaskSchedulerEventListener implements ApplicationListener<CancelTaskSchedulerEvent> {

    private final CancelableThreadPoolTaskScheduler taskScheduler;

    @Override
    public void onApplicationEvent(@NonNull CancelTaskSchedulerEvent event) {
        taskScheduler.cancelFutureSchedulerTasks((String) event.getSource());
    }
}