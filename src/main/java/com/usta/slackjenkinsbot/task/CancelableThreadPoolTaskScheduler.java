package com.usta.slackjenkinsbot.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@Component
public class CancelableThreadPoolTaskScheduler extends ThreadPoolTaskScheduler {

    Logger logger = LoggerFactory.getLogger(CancelableThreadPoolTaskScheduler.class);

    private Map<Object, ScheduledFuture<?>> scheduledTasks = new HashMap<>();

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
        ScheduledFuture<?> future = super.scheduleAtFixedRate(task, period);
        scheduledTasks.put(task, future);
        return future;
    }

    public void cancelFutureSchedulerTasks(String cancelReason) {
        scheduledTasks.forEach((k, v) -> v.cancel(false));
        logger.info(cancelReason);
    }
}
