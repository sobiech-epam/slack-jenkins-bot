package com.usta.slackjenkinsbot.task;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

@Component
public class TaskExecutor {

    private final long pollingRate;
    private final TaskScheduler taskScheduler;

    public TaskExecutor(@Value("${job.status.polling.rate}") long pollingRate,
                        TaskScheduler taskScheduler) {
        this.pollingRate = pollingRate;
        this.taskScheduler = taskScheduler;
    }

    public void executeJob(Runnable job) {
        taskScheduler.scheduleAtFixedRate(job, pollingRate);
    }
}
